//
//  main.m
//  labs-steamaudio
//
//  Created by pir2MM on 12/13/2556 BE.
//  Copyright (c) 2556 pir2MM. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

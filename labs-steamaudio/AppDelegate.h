//
//  AppDelegate.h
//  labs-steamaudio
//
//  Created by pir2MM on 12/13/2556 BE.
//  Copyright (c) 2556 pir2MM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
